﻿using System.ComponentModel.DataAnnotations;

namespace WebViewModel.Models
{
    public class Book
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string ISBN { get; set; }
        [Display(Name = "#")]
        public int numberOfPages { get; set; }
        public Author Author { get; set; }
    }

    
}
